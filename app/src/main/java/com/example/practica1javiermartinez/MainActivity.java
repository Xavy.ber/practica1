package com.example.practica1javiermartinez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Button buttonLogin;
    Button buttonGuardar;
    Button buttonBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonGuardar = findViewById(R.id.buttonGuardar);
        buttonBuscar = findViewById(R.id.buttonBuscar);
        buttonLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( MainActivity.this, login_activity.class);
                startActivity(intent);

            }
        });
        buttonGuardar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( MainActivity.this, guardar_activity.class);
                startActivity(intent);

            }
        });


        buttonBuscar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( MainActivity.this, buscar_activity.class);
                startActivity(intent);

            }
        });

    }
}